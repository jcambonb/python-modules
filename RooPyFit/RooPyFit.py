"""
Module designed by Ivan Cambon Bouzas 
PhD student at Instituto Galego de Fisica de Altas Enerxias (IGFAE)
This module includes functions that are thought as shortcuts for the RooFit work in python
Any doubt or suggestion, check https://gitlab.cern.ch/jcambonb
"""

import ROOT
import numpy as np
import uncertainties as un
import pandas as pd

"""
Basic function for plotting a histogram Roofit into a TCanvas object
-var: RooRealVar object
-data: RooDataSet or RooDataHist object
-model: RooAbsPdf object
-bool_comp: Python bool
-comp: Python List whose components are RooFit pdf
-colors: Python List whose components are TColor objects
-xlabel: Python String for xlabel
-ylabel: Python String for ylabel
-title: Python String for title
"""
def modelplot(var, data, model, 
              color=ROOT.kBlue, comps=None, comps_color=None, comps_style=None, 
              xlabel="mass", ylabel="N_{events}", title="title",
              lgd=None, lgd_comp_names=None, lgd_comp_styles=None):
    
    fr = var.frame()
    data.plotOn(fr)

    if comps:
        if comps_style:
            for i in range(len(comps)):
                model.plotOn(fr, 
                             ROOT.RooFit.Components(comps[i]), 
                             ROOT.RooFit.Name(comps[i]),
                             ROOT.RooFit.LineColor(comps_color[i]), 
                             ROOT.RooFit.LineStyle(comps_style[i]))
                
                model.plotOn(fr, 
                             ROOT.RooFit.Name(model.GetName()), 
                             ROOT.RooFit.LineColor(color))

        else:
            for i in range(len(comps)):
                model.plotOn(fr, 
                             ROOT.RooFit.Components(comps[i]), 
                             ROOT.RooFit.Name(comps[i]),
                             ROOT.RooFit.LineColor(comps_color[i]))
                
                model.plotOn(fr, 
                             ROOT.RooFit.Name(model.GetName()), 
                             ROOT.RooFit.LineColor(color))
    else:
        model.plotOn(fr, 
        ROOT.RooFit.Name(model.GetName()), 
        ROOT.RooFit.LineColor(color))
        
    fr.GetXaxis().SetTitle(xlabel)
    fr.GetYaxis().SetTitle(ylabel)
    fr.SetTitle(title)
    fr.Draw()
    
    if lgd:
        lgd.AddEntry(fr.findObject(data.GetName()), "Data", "pe")
        
        if comps:
            for i in range(len(comps)):
                lgd.AddEntry(fr.findObject(comps[i]), lgd_comp_names[i], lgd_comp_styles[i])
        
        lgd.AddEntry(fr.findObject(model.GetName()), "Fit", "l")
        lgd.SetBorderSize(0)
        lgd.SetFillStyle(0)
        lgd.Draw()

    print("chi2/bins = {0}".format(fr.chiSquare()))


"""
Function that plots the Pull [(x-mean)/error] histogram of a fit. 
Designed for TCanvas objects with two TPad in horizontal position
-var: RooRealVar object
-data: RooDataSet or RooDataHist object
-model: RooAbsPdf object
"""
def pullplot(var, dh, model):
    fr1 = var.frame()

    dh.plotOn(fr1)
    model.plotOn(fr1)

    histo_pulls = fr1.pullHist()
    histo_pulls.SetFillColor(ROOT.kBlack)
    histo_pulls.SetLineWidth(0)
    histo_pulls.SetMarkerSize(0)

    fr2 = var.frame()
    fr2.addPlotable(histo_pulls, "B")
    fr2.SetTitle("")
    fr2.GetXaxis().SetTitle("")
    fr2.GetXaxis().SetLabelSize(0)
    fr2.GetYaxis().SetNdivisions(5)
    fr2.GetYaxis().SetLabelSize(0.2)
    fr2.GetYaxis().SetTitleSize(0.3)
    fr2.GetYaxis().SetTitleOffset(0.2)
    fr2.GetYaxis().SetTitle("#it{#Delta/#sigma}")
    fr2.Draw()

"""
Function for creating a TCanvas with the plot of both fit and pullplot. 
Designed for lhcbStyle.C
-var: RooRealVar object
-data: RooDataSet or RooDataHist object
-model: RooAbsPdf object
-bool_comp: Python bool
-comp: Python List whose components are RooFit pdf
-colors: Python List whose components are TColor objects
-xlabel: Python String for xlabel
-ylabel: Python String for ylabel
-title: Python String for title
-file_name: Python String for the output file of the plot
"""
def plot(var, data, model, 
         color=ROOT.kBlue, comps=None, comps_color=None, comps_style=None, 
         xlabel="mass", ylabel="N_{events}", title="title",
         lgd=None, lgd_comp_names=None, lgd_comp_styles=None,
         file_name=None):    
    
    c = ROOT.TCanvas()
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.2, 1, 1)
    pad1.Draw()
    pad1.cd()
    modelplot(var, data, model, 
              color=color, comps=comps, comps_color=comps_color, comps_style=comps_style, 
              xlabel=xlabel, ylabel=ylabel, title=title,
              lgd=lgd, lgd_comp_names=lgd_comp_names, lgd_comp_styles=lgd_comp_styles)
    
    c.cd()
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.01, 1, 0.2)
    pad2.SetBottomMargin(0.1)
    pad2.Draw()
    pad2.cd()
    pullplot(var, data, model)
    c.Draw()
    
    if file_name:
        c.SaveAs(file_name)

"""
Function for creating a TCanvas with the plot of 2 fits and pullplots for both of them. 
Designed for lhcbStyle.C
-var: RooRealVar object
-data: RooDataSet or RooDataHist object
-model: RooAbsPdf object
-bool_comp: Python bool
-comp: Python List whose components are RooFit pdf
-colors: Python List whose components are TColor objects
-xlabel: Python String for xlabel
-ylabel: Python String for ylabel
-title: Python String for title
-file_name: Python String for the output file of the plot
"""
def subplot(var1, data1, model1, var2, data2, model2,
            color1=ROOT.kBlue, comps1=None, comps_color1=None, comps_style1=None, 
            xlabel1="mass", ylabel1="N_{events}", title1="title",
            lgd1=None, lgd_comp_names1=None, lgd_comp_styles1=None,
            color2=ROOT.kBlue, comps2=None, comps_color2=None, comps_style2=None, 
            xlabel2="mass", ylabel2="N_{events}", title2="title",
            lgd2=None, lgd_comp_names2=None, lgd_comp_styles2=None,
            file_name=None):
    
    c = ROOT.TCanvas("", "", 1200, 800)
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.2, 0.49, 1)
    pad1.Draw()
    pad1.cd()
    
    modelplot(var1, data1, model1, 
              color=color1, comps=comps1, comps_color=comps_color1, comps_style=comps_style1, 
              xlabel=xlabel1, ylabel=ylabel1, title=title1,
              lgd=lgd1, lgd_comp_names=lgd_comp_names1, lgd_comp_styles=lgd_comp_styles1)

    c.cd()
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.01, 0.49, 0.2)
    pad2.SetBottomMargin(0.1)
    pad2.Draw()
    pad2.cd()
    pullplot(var1, data1, model1)

    c.cd()
    pad3 = ROOT.TPad("pad3", "pad3", 0.51, 0.2, 1.0, 1)
    pad3.Draw()
    pad3.cd()
    modelplot(var2, data2, model2, 
              color=color2, comps=comps2, comps_color=comps_color2, comps_style=comps_style2, 
              xlabel=xlabel2, ylabel=ylabel2, title=title2,
              lgd=lgd2, lgd_comp_names=lgd_comp_names2, lgd_comp_styles=lgd_comp_styles2)
    
    c.cd()
    pad4 = ROOT.TPad("pad4", "pad4", 0.51, 0.01, 1.0, 0.2)
    pad4.SetBottomMargin(0.1)
    pad4.Draw()
    pad4.cd()
    pullplot(var2, data2, model2)
    c.Draw()
    
    if file_name:
        c.SaveAs(file_name)
        
    
"""
Class Fit for working with RooFitResult objects more pythonic

-r: RooFitResult object
"""
class Fit:
    """
    Atributtes:
    -result: RooFitResult object
    """
    def __init__(self,r):
        self.result = r
        
    """
    Methods
    """
    
    """
    Returns the parameters of the fit in a np.array as RooRealVariables
    """ 
    def parameters_RRV(self):
        r = self.result
        a = np.array(r.floatParsFinal())
        return a
    
    """
    Gets the i parameter of the fit
    -i: int lower that the number of parameters
    """
    def __getitem__(self, i):
        return (self.parameters_RRV())[i]
    
    """
    Number of parameters
    """
    def __len__(self):
        return len(self.parameters_RRV())
    
    """
    Defines the string
    """
    def __str__(self):
        string = "RooFit Result Object" + '\n'
        string = string + "Parameters=" + str(self.par_names())
        return string
    
    """
    Returns a list with the names of the parameters
    """
    def par_names(self):
        names = [self[i].GetName() for i in range(len(self))]
        return names
    
    """
    Returns a list with the values of the parameters
    """
    def par_values(self):
        values = [self[i].getVal() for i in range(len(self))]
        return values

    """
    Returns a list with the errors of the parameters
    """
    def par_errors(self):
        values = [self[i].getError() for i in range(len(self))]
        return values

    """
    Function that returns in terminal the fit interesting results.
    """
    def print(self):
        r = self.result
        print("------------------------------------------------")
        print("Fit status = {0}".format(r.status()))
        print("EDM = {0}".format(r.edm()))
        print("-log(L) minimum = {0}".format(r.minNll()))
        print("final value of floating parameters")
        r.floatParsFinal().Print("s")
        
        print("correlation matrix")
        cor = r.correlationMatrix()
        cor.Print()
        print("covariance matrix")
        cov = r.covarianceMatrix()
        cov.Print()
        print("------------------------------------------------")

    """
    Saving RooFitResult values into txt
    -file_name: str which corresponds to the name of the new file
    """
    def save_to_csv(self, 
                    file_name="fit_parameters.csv"):
        
        fit_result = {"Parameters": np.array(self.par_names()),
                      "Values": np.array(self.par_values()),
                      "Errors": np.array(self.par_errors())}

        fit_result = pd.DataFrame(fit_result)
        
        fit_result.to_csv(file_name, index=False)

    """
    Saving RooFitResult into root file
    -file_name: str which corresponds to the name of the new file
    """
    def save_to_root(self, 
                     file_name="fit_result.root"):
        
        t = ROOT.TFile(file_name, "RECREATE")
        # Note that we write the RooFitResult object with the same name as root file
        a = self.result
        a.Write(file_name.replace(".root", ""))
        t.Close()

    """
    Function that saves a RooFitResult as a LaTeX table
    Modules numpy, pandas and uncertainties are required
    -file_name: str for the .tex name
    -fit_type: str which stablish if the fit was binned ("b") or unbinned ("u")
    -data: RooDataSet or RooDataHist object
    -var: RooRealVar of the fit
    -model: RooAbsPdf of the fit
    -units: units of the parameters
    """
    def save_to_latex(self, var, data, model, units,
                      fit_type="b", bins=100,
                      file_name="fit_parameters.tex"):
        
        values = np.array(self.par_values())
        errors = np.array(self.par_errors())
        names = np.array(self.par_names())

        un_objects = np.array([un.ufloat(values[i],errors[i]) for i in range(len(errors))])
        pm_values = np.array(['${:L}$'.format(un_objects[i]) for i in range(len(un_objects))])

        if fit_type == "b":
            Chi2_model = ROOT.RooChi2Var("chi2", "chi2", model, data)
            Chi2_val = Chi2_model.getVal()
            pm_values = np.append(pm_values,'{:.2f}'.format(Chi2_val/bins))
            names = np.append(names ,u"$\chi^2$/bins")

        else:
            fr = var.frame()
            data.plotOn(fr)
            model.plotOn(fr)
            Chi2_val = fr.chiSquare()
            pm_values = np.append(pm_values,'{:.2f}'.format(Chi2_val))
            names = np.append(names, u"$\chi^2$/bins")
    
        units = np.append(units, "No units")

        table = np.stack((names, pm_values, units))

        df = pd.DataFrame(table.T, columns=["Parameters", "Values", "Units"])

        df.to_latex(file_name, index=False, column_format='ccc', escape=False)

    """
    Read a root file and get the RooFitResult object
    The object had to be save in the TFile with the same name as the .root file
    -file_name: str name of the root file
    """
    def read_root(file_name):
        file = ROOT.TFile(file_name, "READ")
        r = file.Get(file_name.replace(".root", ""))
        r.SetDirectory(ROOT.nullptr)
        return Fit(r)
        

    """
    Function that plots the covariance ellipse of two fit parameters var1 and var2
    -var1: RooRealVar defined in Fit object
    -var2: RooRealVar defined in Fit object
    -n: float object. Defines the limit of the plot. Multiplies the error values
    """
    def MatrixCov_2D(self, var1, var2, n):
        val1 = var1.getVal()
        val2 = var2.getVal()

        u1 = var1.getError()
        u2 = var2.getError()

        frame = ROOT.RooPlot(var1, var2, (val1 - n*u1), (val1 + n*u1), (val2 - n*u2), (val2 + n*u2))
        frame.SetTitle("Covariance between" + " " + var1.GetName() + " " + "and" + " " + var2.GetName())
        (self.result).plotOn(frame, var1, var2, "ME12ABHV")
        frame.Draw()

    """
    Function that gives the correlation between two parameters. A option for correlation matrix 2D plot is set
    -var1: RooRealVar defined in Fit object
    -var2: RooRealVar defined in Fit object
    -bool: bool object. If it is True, the 2D matrix is plotted in a TCanvas 
    """
    def MatrixCorr_vals(self, var1, var2, bool):
        r = self.result
        print("Correlation between" + " " + var1.GetName() + " " + "and" + " " + var2.GetName() + "=", r.correlation(var1, var2))
    
        if bool: 
            hcorr = r.correlationHist()
            hcorr.Draw("colz")

