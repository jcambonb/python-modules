"""
Module designed by Ivan Cambon Bouzas 
PhD student at Instituto Galego de Fisica de Altas Enerxias (IGFAE)
This module includes functions that are useful for Maching Learning in PyROOT
Any doubt or suggestion, check https://gitlab.cern.ch/jcambonb
"""

import mplhep
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import auc, roc_curve
from sklearn.metrics import precision_recall_curve

"""
Function that plots a variable 
-var: str. The name of the variable that we want to plot
-df: pd.DataFrame.  data
-bins: int. Number of bins of the histogram
-label: str. Label of the variable
-color: str. Color of th plot
-density: Bool. If the histogram is normalized or not
"""
def plot(var, df, bins=100,label=None, color=None, density=False, weights=None):

    hist, bins = np.histogram(df[var], bins=bins, density=density, weights=weights)

    mplhep.histplot((hist, bins), label=label, color=color)
    plt.xlim(bins[0], bins[-1])
    plt.legend(loc='best')


"""
Function that plots the ROC curve of a given classifier
-alg: scikit learn classifier object. It must be trainend previously
-data: pd.DataFrame object that corresponds to the input features and the category label (train or test)
-training_features: list of str. It contains the training feature names
-category: str. Is the number of the bool column used to label each datatype
-label: str for the label of the curve 
"""
def roc_curve_plot(alg, data, training_features, category, color=None, label=None):
    y_score = alg.predict_proba(data[training_features])[:,1]
    fpr, tpr, thresholds = roc_curve(data[category], y_score)
    area = auc(fpr, tpr)

    plt.plot([0, 1], [0, 1], color='black', linestyle='--')

    if label:
        plt.plot(fpr, tpr, label=f'{label} ROC curve (area = {area:.2f})', color=color)
    else:
        plt.plot(fpr, tpr, label=f'ROC curve (area = {area:.2f})',color=color)

    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc='lower right')
    plt.gca().set_aspect('equal', adjustable='box')


"""
Function that plots the Precision vs Recall curve of a given classifier
-alg: scikit learn classifier object. It must be trained previously
-training_data: pd.DataFrame object that corresponds to the train set
-training_columns: list of str. It contains the training feature names
-category: str. Is the number of the bool column used to label each datatype
-label: str for the label of the curve 
"""
def precision_recall_plot(alg, data, training_features, category, color=None, label=None):
    y_score = alg.predict_proba(data[training_features])[:,1]
    precisions, recalls, thresholds = precision_recall_curve(data[category], y_score)

    plt.plot(recalls, precisions, label=label,color=color)
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.xlabel('Precision')
    plt.ylabel('Recall')
    
    if label:
        plt.legend(loc='best')

    plt.gca().set_aspect('equal', adjustable='box')


"""
Function that plots the Figure of Merits (FoM) in order to get the optimal cut of the alg variable
-alg: scikit learn classifier object. It must be trainend previously
-data: pd.DataFrame object that corresponds to the input features and the category label (train or test)
-training_features: list of str. It contains the training feature names
-category: str. Is the number of the bool column used to label each datatype
-nsig: float. Number of signal events inside the side bands
-nbkg: float. Number of background events inside the side bands
-label: str for the label of the curve
-color: str for the color of th plot
"""
def FoM_plot(alg, data, training_features, category, nsig, nbkg, label=None, color=None):
    y_score = alg.predict_proba(data[training_features])[:,1]
    fpr, tpr, thresholds = roc_curve(data[category], y_score)

    n_sig = nsig
    n_bkg = nbkg
    S = n_sig * tpr
    B = n_bkg * fpr
    
    metric = np.array([S[i] / np.sqrt(S[i] + B[i]) for i in range(len(S)) if S[i] != 0 or B[i] != 0])
    thresholds = np.array([thresholds[i] for i in range(len(thresholds)) if S[i] != 0 or B[i] != 0])

    plt.plot(thresholds, metric, label=label, color=color)
    plt.xlabel('BDT cut value')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')

    if label:
        plt.legend(loc='best')

    optimal_cut = thresholds[np.argmax(metric)]
    plt.axvline(optimal_cut, color='black', linestyle='--')   
    print(f'The optimal cut value is {optimal_cut:.2f} with and S/sqrt(S+B) of {np.max(metric):.2f}')


"""
Function that plots the feature importance of the a given trained algorithm
-alg: scikit learn ml algorithm object. It must be trained previously
-training_features_names: list. Its elements will be the ticks of the plot
"""
def feature_importance_plot(alg, training_features_names, color=None):
    importances = list(alg.feature_importances_)
    tagging = sorted(zip(importances,training_features_names), reverse=True)

    order_import = [tagging[i][0] for i in range(len(tagging))]
    order_names = [tagging[i][1] for i in range(len(tagging))]

    plt.figure()
    plt.barh(range(len(order_import)), order_import, color=color, tick_label=order_names)
    plt.xlabel("Importance")


"""
Function that prints the feature importance of the a given trained algorithm
-alg: scikit learn ml algorithm object. It must be trained previously
-training_features_names: list. Its elements will be the ticks of the plot
"""
def feature_importance_print(alg, training_features_names):
    importances = list(alg.feature_importances_)
    tagging = sorted(zip(importances, training_features_names), reverse=True)

    order_import = [tagging[i][0] for i in range(len(tagging))]
    order_names = [tagging[i][1] for i in range(len(tagging))]

    df = pd.DataFrame({"importance":order_import}, index=order_names)
    print(df)    


"""
Function that compares the behavoir of the ML algorithm in train and test sets
-alg: scikit learn classifier object. It must be trainend previously
-train_set: pd.DataFrame object that corresponds to the train set
-test_set: pd.DataFrame object that corresponds to the test set
-training_columns: list of str. It contains the training feature names
-category: str. Is the number of the bool column used to label each datatype
-label: int. Number of bins for the corresponding histgram
"""
def compare_train_test(alg, train_set, test_set ,training_features, category, bins=30):
    decisions = []

    X_train = train_set[training_features]; y_train = train_set[category]
    X_test = test_set[training_features]; y_test = test_set[category]

    for X,y in ((X_train, y_train), (X_test, y_test)):
        d1 = alg.decision_function(X[y>0.5]).ravel()
        d2 = alg.decision_function(X[y<0.5]).ravel()
        decisions += [d1, d2]
        
    low = min(np.min(d) for d in decisions)
    high = max(np.max(d) for d in decisions)
    low_high = (low,high)
    
    plt.hist(decisions[0],
             color='r', alpha=0.5, range=low_high, bins=bins,
             histtype='stepfilled', normed=True,
             label='S (train)')
    
    plt.hist(decisions[1],
             color='b', alpha=0.5, range=low_high, bins=bins,
             histtype='stepfilled', normed=True,
             label='B (train)')

    hist, bins = np.histogram(decisions[2],
                              bins=bins, range=low_high, normed=True)
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale
    
    width = (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    plt.errorbar(center, hist, yerr=err, fmt='o', c='r', label='S (test)')
    
    hist, bins = np.histogram(decisions[3],
                              bins=bins, range=low_high, normed=True)
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    plt.errorbar(center, hist, yerr=err, fmt='o', c='b', label='B (test)')

    plt.xlabel("Classifier utput")
    plt.ylabel("Arbitrary units")
    plt.legend(loc='best')

    
