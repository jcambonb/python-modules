# Python Modules: Easy work with PyROOT analysis
 By Ivan Cambon Bouzas

## Introduction

In this repository we include a list of personal modules designed for data analysis with PyROOT. The motivation of this project is to create a nice workspace for working with ROOT in python and make easier the HEP analysis. Each module is focused on defining functions and classes which work internally with the ROOT objects and their methods. That allows us an efficient and fluid coding closer to python and not as monolotic as ROOT.

## Requirements

In order to run the codes a installation of python with ROOT is required. For LHC$`b`$ members, the lb-conda environment has all the needed packages for the analysis. If is not the case, the packages required are:

1. ROOT
2. numpy
3. matplotlib.pyplot
4. pandas
5. uncertanties
6. scikit-learn

Moreover, for plotting, the RooPyFit functions are designed for LHCb plot style. So, it is recommended to use the lhcbStyle.C macro through lb-conda or installing it on your own python environment. If not, the structure of the plots will be slightly different and changes should be necessary.

## Actual modules

1. **RooPyShort**

This module includes functions that makes more pythonic the object oriented coding in ROOT (i.e TCanvas plots). Implementations between RDataFrame and pandas DataFrame are included too. More specific functions and classes are included for getting event information from ROOT files. Finally, it is included a dictionary that has all the required MCMatching conditions for $D_{sJ}$ production research.

2. **RooPyFit**

A more complete module designed for working with RooFit, a powerful fit environment but with his own plotting and result syntax. Here we define several functions to perform plots and the class Fit, which works with the fit result objects of RooFit and includes fancy methods for printing the result or saving it in a table with LaTeX format.

3. **RooPyML**

A specific module oriented for maching learning applied in general LHCb analysis. It includes functions to plot variable, specific functions to check the performance of a given algorithm and FoM computation for classifier output cut optimization. Now it works for **scikit-learn** algorithms, but improvements to TMVA or Pytorch are considered.