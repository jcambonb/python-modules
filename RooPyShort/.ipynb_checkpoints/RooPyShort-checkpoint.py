"""
Module designed by Ivan Cambon Bouzas 
PhD student at Instituto Galego de Fisica de Altas Enerxias (IGFAE)
This module includes functions that are thought as shortcuts for ROOT coding in python
Any doubt or suggestion, check https://gitlab.cern.ch/jcambonb
"""

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import vector as vec

ROOT.gROOT.ProcessLine(".L /home3/ivan.cambon/Cpp_Macros/HEP_Exp_Tools/Kinematics.C")

"""
Function for converting RDataFrame into pandas DataFrame
-tdf: RDataFrame object
-columns: iterable which elements correspond to branches of the RDataFrame. It is recommended to not use all the branches.
"""
def rdf_to_pdf(tdf, columns):
    np_tdf = tdf.AsNumpy(columns)
    pdf_tdf = pd.DataFrame(np_tdf)
    return pdf_tdf

"""
Function for converting pandas DataFrame into RDataFrame
-pdf: Pandas DataFrame object
"""
def pdf_to_rdf(pdf):
    columns = pdf.columns
    np_pdf = {key: pdf[key].values for key in columns}
    tdf = ROOT.RDF.FromNumpy(np_pdf)
    return tdf

"""
Function which defines over a tdf class object a variable call Ds0_TRUE_M, the kinematic true mass of Dspi0 final state
-rdf: RDataFrame object
"""
def Ds0_TRUE_MM_def(rdf):
    rdf = rdf.Define("Ds0_TRUE_M", "mass(Ds_TRUEP_X + piz_TRUEP_X, Ds_TRUEP_Y + piz_TRUEP_Y, Ds_TRUEP_Z + piz_TRUEP_Z, Ds_TRUEP_E + piz_TRUEP_E)")
    rdf = rdf.Define("piz_TRUE_M", "mass(gamma_1_TRUEP_X + gamma_2_TRUEP_X, gamma_1_TRUEP_Y + gamma_2_TRUEP_Y, gamma_1_TRUEP_Z + gamma_2_TRUEP_Z, gamma_1_TRUEP_E + gamma_2_TRUEP_E)")
    return rdf  

"""
Function which creates a variable all nCandidate2, a new version of nCandidate once a selection is made in RDataFrames created with LHCb nTuples
-tdf: RDataFrame Object
-columns: List of str which corresponds to a little set of variables included in tdf. If columns is large, it will take a while to run
"""
def nCandidate2_def(tdf, columns):
    np_tdf = tdf.AsNumpy(columns)
    runs_and_evts = list(zip(np_tdf['runNumber'], np_tdf['eventNumber']))
        
    count_dict = {}
    for x in runs_and_evts:
        count_dict[x] = count_dict.get(x, 0) + 1

    cand2_array = np.array([])
    totC2_array = np.array([])
    
    for x in count_dict.keys():
        counts = count_dict[x]
        aux1 = np.arange(counts)
        aux2 = counts * np.ones(counts)
        
        cand2_array = np.append(cand2_array, aux1)
        totC2_array = np.append(totC2_array, aux2)

    np_tdf["nCandidate2"] = cand2_array.astype("int")
    np_tdf["totCandidates2"] = totC2_array.astype("int")
    
    tdf_new = ROOT.RDF.FromNumpy(np_tdf)

    return tdf_new

"""
Function which perform a Figure of Merits (FoM) for 1 variable in order to get its optimal cut through the signifiance (S/sqrt(S+B)) criteria
Returns plots of significance and purity (S/B)
-sig_rdf: RDataFrame object which corresponds to the signal dataset
-bkg_rdf: RDataFrame object which corresponds to the background dataset
-var: str which is the name of the variable that appears in the nTuples
-estimator: iterable object of 2 elements. First one is the signal events inside de sidebands cuts. Second one is the background events inside de sidebands cuts
-var_threshold: float. The max value of the variable where the FoM is calculated
-ndots: int object. Sets the number of cuts to be tested
-cut_type: str object which sets the type of cut (greater or lower)
-xlabel: str object. It will be the xlabel of the FoM plots
"""
def FoM1D_plot(sig_rdf, bkg_rdf, var, estimator, var_threshold, ndots=30, cut_type=">", xlabel="feature"):
    var_min = min(sig_rdf.Min(var).GetValue(), bkg_rdf.Min(var).GetValue())
    var_max = max(sig_rdf.Max(var).GetValue(), bkg_rdf.Max(var).GetValue())

    var_vals = np.linspace(var_min, var_threshold, ndots)

    if cut_type == ">":
      var_cuts = [var+">"+str(var_vals[i]) for i in range(len(var_vals))]

    if cut_type == "<":
      var_cuts = [var+"<"+str(var_vals[i]) for i in range(len(var_vals))]

    Sig_raw = sig_rdf.Count().GetValue()
    Bkg_raw = bkg_rdf.Count().GetValue()

    Sig_rdf_cut = [sig_rdf.Filter(var_cuts[i]).Histo1D(("", "", 100, var_min, var_max), var) for i in range(len(var_cuts))]
    Bkg_rdf_cut = [bkg_rdf.Filter(var_cuts[i]).Histo1D(("", "", 100, var_min, var_max), var) for i in range(len(var_cuts))]
    
    sig_prob = np.array([Sig_rdf_cut[i].Integral() / Sig_raw for i in range(len(Sig_rdf_cut))])
    bkg_prob = np.array([Bkg_rdf_cut[i].Integral() / Bkg_raw for i in range(len(Bkg_rdf_cut))])

    S = estimator[0] * sig_prob
    B = estimator[1] * bkg_prob
    Significance = S / np.sqrt(S+B)
    Purity = S / B

    i = np.argmax(Significance)
    f = np.argmax(Purity)

    print(var + " cut for maximum significance", var_cuts[i])
    print(var + " cut for maximum purity", var_cuts[f])

    plt.figure(1)
    plt.plot(var_vals,Significance, 'b.', label=r"$\frac{S}{\sqrt{S+B}}$")
    plt.xlabel(xlabel)
    plt.ylabel(r'Figure of Merits', fontsize=14)
    plt.title(var + ' rectangular cut optimization')
    plt.legend()
    plt.savefig("Exp_MagD18_" + var + "_cut_FoM_sig.pdf", dpi=300, bbox_inches='tight')


"""
Basic function for histogram TH1D plotting. Line or Marker options are included
For RooFit plotting check the RooPyFit module
-hist: TH1D object
-xlabel: str for xlabel
-ylabel: str for ylabel
-title: str for title
-option: str which corresponds to Draw options of the TH1D. Only 2 are valid: 'E same' and 'same'
-stycor: Iterable of len=2. Element [0] is the style option and element [1] is the color option
-norm: Bool. If it is True, the histogram will be plotted as a frequency one
"""
def TH1D_plot(histo, xlabel="feature", ylabel="A.U", title="title", option="same", stycor=[20, 1], norm=False):
    histo.GetXaxis().SetTitle(xlabel)
    histo.GetYaxis().SetTitle(ylabel)
    histo.SetTitle(title)
    histo.SetMinimum(0)

    if "E" in option:
        histo.SetMarkerStyle(stycor[0])
        histo.SetMarkerColor(stycor[1])

    if option == "same" or option == "HIST same":
        histo.SetLineStyle(stycor[0])
        histo.SetLineColor(stycor[1])

    if norm:
        histo.DrawNormalized(option)
    else:
        histo.Draw(option)


"""
Basic function for histogram TH2D plotting. SCAT or BOX options are included
For RooFit plotting check the RooPyFit module
-hist: TH2D object
-xlabel: str for xlabel
-ylabel: str for ylabel
-title: str for title
-option: str which corresponds to Draw options of the TH2D.
-stycor: Iterable of len=2. Element [0] is the style option and element [1] is the color option. Only valid for SCAT and BOX styles
-freq: Bool. If it is True, the histogram will be plotted as a frequency one
"""
def TH2D_plot(histo, xlabel="feature1", ylabel="feature2", title="title", option="CONT same", stycor="", norm=False):
    histo.GetXaxis().SetTitle(xlabel)
    histo.GetYaxis().SetTitle(ylabel)
    histo.SetTitle(title)

    if option == 'SCAT same':
        histo.SetMarkerStyle(stycor[0])
        histo.SetMarkerColor(stycor[1])  

    if option == 'BOX same':
        histo.SetFillColor(stycor[1])

    if norm:
        histo.DrawNormalized(option)
    else:
        histo.Draw(option)
   
"""
Basic function for histogram TProfile plotting. Line or Marker options are included
For RooFit plotting check the RooPyFit module
-prof: TProfile object
-xlabel: str for xlabel
-ylabel: str for ylabel
-title: str for title
-option: str which corresponds to Draw options of the TProfile.
-stycor: Iterable of len=3. 
    *stycor[0]: ROOT marker style
    *stycor[1]: ROOT marker color.
    *stycor[2]: ROOT line color
"""
def TProfile_plot(prof, xlabel="feature1", ylabel="feature2", title="title", option="E1 same", stycor=[20, 1, 1]):
    prof.GetXaxis().SetTitle(xlabel)
    prof.GetYaxis().SetTitle(ylabel)
    prof.SetTitle(title)

    prof.SetMarkerStyle(stycor[0])
    prof.SetMarkerColor(stycor[1])
    prof.SetLineColor(stycor[2])  

    prof.Draw(option)
    
"""
Basic function for TF1 fitting and plotting. Line or Marker options are included
For RooFit plotting check the RooPyFit module
-fun: TF1 object
-data: ROOT object with Fit attribute: TGraph, TH1D, TProfile, etc.
-stycor: Iterable of len=3. 
    *stycor[0]: ROOT line style
    *stycor[1]: ROOT line color
    *stycor[2]: ROOT line width
"""
def curve_fit(fun, data, stycor=[1, 1, 1]):
    data.Fit(fun)
    
    f = data.GetFunction(fun.GetName())
    f.SetLineStyle(stycor[0])
    f.SetLineColor(stycor[1])
    f.SetLineWidth(stycor[2])
    
"""
Function for legend plotting in usual TCanvas. 
For RooFit plotting check the RooPyFit module
-Tlgd: TLegend object
-hlist: iterable object whose elements are Plotable objects. For TH1D comming for RDataFrame, you must use h.GetPtr()
-lgd_names: iterable object whose elements are the names that we want to put in the legend
-styles: iterable object whose elements are the styles of the legend
"""
def legend_plot(Tlgd, hlist, lgd_names, styles):
    for i in range(len(hlist)):
        Tlgd.AddEntry(hlist[i], lgd_names[i], styles[i])

    Tlgd.SetBorderSize(0)
    Tlgd.SetFillStyle(0)
    Tlgd.Draw()


"""
Function to save a ROOT histogram inside a root file. We use the name of the file without .root as the name of the histogram saved inside the file
-file_name: str object. Name of the file without .root
-hist: TH1 object
"""
def save_hist_to_root(file_name, hist):
    t = ROOT.TFile.Open(file_name + ".root", "RECREATE")
    hist.Write("MyHist")
    t.Close()

"""
Function to read a ROOT histogram inside a root file. The name of the histogram must be the same as the file name without .root
-file_name: str object. Name of the root file
"""
def read_hist_to_root(file_path):
    file = ROOT.TFile(file_path, "READ")
    hist = file.Get("MyHist")
    
    if hist:
       return hist
    else:
        print(f"Histogram not found in '{file_path}'.")
        return None
       

"""
Function that has all the TRUEIDs matching for the particles in our analysis
"""
def TRUEIDs():

    # DsPi02ggTuple and DsstGammaTuple
    #=============================================================================================================================================================================================================================
    # Ds0DsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_Ds0DsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 10431"
    Kmi_ID_Ds0DsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 10431"
    Kpl_ID_Ds0DsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 10431"
    piz_ID_Ds0DsPi0 = "abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22 && abs(gamma_1_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_GD_MOTHER_ID) == 10431 && abs(gamma_1_MC_GD_MOTHER_ID) == 10431"
  
    TRUEID_DsPi02ggTuple_Ds0DsPi0 = pi_ID_Ds0DsPi0 + "&&" + Kmi_ID_Ds0DsPi0 + "&&" + Kpl_ID_Ds0DsPi0 + "&&" + piz_ID_Ds0DsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # DsstDsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsstDsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DsstDsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DsstDsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    piz_ID_DsstDsPi0 = "abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22 && abs(gamma_1_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_GD_MOTHER_ID) == 433 && abs(gamma_1_MC_GD_MOTHER_ID) == 433"

    TRUEID_DsPi02ggTuple_DsstDsPi0 = pi_ID_DsstDsPi0 + "&&" + Kmi_ID_DsstDsPi0 + "&&" + Kpl_ID_DsstDsPi0 + "&&" + piz_ID_DsstDsPi0  
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
    # Ds1DsstPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_Ds1DsstPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433 && abs(pi_MC_GD_GD_MOTHER_ID) == 20433"
    Kmi_ID_Ds1DsstPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433 && abs(Kmi_MC_GD_GD_MOTHER_ID) == 20433"
    Kpl_ID_Ds1DsstPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433 && abs(Kpl_MC_GD_GD_MOTHER_ID) == 20433"
    gg_ID_Ds1DsstPi0 = "abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22 && abs(gamma_2_MC_GD_MOTHER_ID) == 20433 && abs(gamma_1_MC_GD_MOTHER_ID) == 20433"


    TRUEID_DsPi02ggTuple_Ds1DsstPi0 = pi_ID_Ds1DsstPi0 + "&&"+Kmi_ID_Ds1DsstPi0 + "&&" + Kpl_ID_Ds1DsstPi0 + "&&" + gg_ID_Ds1DsstPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # DsstDsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsstDsGamma  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DsstDsGamma = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DsstDsGamma = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    gg_ID_DsstDsGamma = "(abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22) && (abs(gamma_1_MC_MOTHER_ID) == 433 | abs(gamma_2_MC_MOTHER_ID) == 433)"


    TRUEID_DsPi02ggTuple_DsstDsGamma = pi_ID_DsstDsGamma + "&&"+Kmi_ID_DsstDsGamma + "&&" + Kpl_ID_DsstDsGamma + "&&" + gg_ID_DsstDsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds1DsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_Ds1DsGamma  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 20433"
    Kmi_ID_Ds1DsGamma = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 20433"
    Kpl_ID_Ds1DsGamma = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 20433"
    gg_ID_Ds1DsGamma = "(abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22) && (abs(gamma_1_MC_MOTHER_ID) == 20433 | abs(gamma_2_MC_MOTHER_ID) == 20433)"


    TRUEID_DsPi02ggTuple_Ds1DsGamma = pi_ID_Ds1DsGamma + "&&"+Kmi_ID_Ds1DsGamma + "&&" + Kpl_ID_Ds1DsGamma + "&&" + gg_ID_Ds1DsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    TRUEID_DsPi02ggTuple = {"Ds0DsPi0": TRUEID_DsPi02ggTuple_Ds0DsPi0, "DsstDsPi0": TRUEID_DsPi02ggTuple_DsstDsPi0, "Ds1DsstPi0": TRUEID_DsPi02ggTuple_Ds1DsstPi0,
                            "Ds1DsGamma": TRUEID_DsPi02ggTuple_Ds1DsGamma, "DsstDsGamma": TRUEID_DsPi02ggTuple_DsstDsGamma}
    #=============================================================================================================================================================================================================================

    # DsGammaTuple
    #=============================================================================================================================================================================================================================
    # DsstDsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsstDsGamma  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DsstDsGamma = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DsstDsGamma = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    gamma_ID_DsstDsGamma = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_MOTHER_ID) == 433"

    TRUEID_DsGammaTuple_DsstDsGamma = pi_ID_DsstDsGamma + "&&" + Kmi_ID_DsstDsGamma + "&&" + Kpl_ID_DsstDsGamma + "&&" + gamma_ID_DsstDsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds1DsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_Ds1DsGamma  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 20433"
    Kmi_ID_Ds1DsGamma = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 20433"
    Kpl_ID_Ds1DsGamma = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 20433"
    gamma_ID_Ds1DsGamma = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_MOTHER_ID) == 20433"

    TRUEID_DsGammaTuple_Ds1DsGamma = pi_ID_Ds1DsGamma + "&&" + Kmi_ID_Ds1DsGamma + "&&" + Kpl_ID_Ds1DsGamma + "&&" + gamma_ID_Ds1DsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds0DsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_Ds0DsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 10431"
    Kmi_ID_Ds0DsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 10431"
    Kpl_ID_Ds0DsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 10431"
    gamma_ID_Ds0DsPi0 = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_GD_MOTHER_ID) == 10431"

    TRUEID_DsGammaTuple_Ds0DsPi0 = pi_ID_Ds0DsPi0 + "&&" + Kmi_ID_Ds0DsPi0 + "&&" + Kpl_ID_Ds0DsPi0 + "&&" + gamma_ID_Ds0DsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # DsstDsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsstDsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DsstDsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DsstDsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    gamma_ID_DsstDsPi0 = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_MOTHER_ID) == 111 && abs(gamma_MC_GD_MOTHER_ID) == 433"

    TRUEID_DsGammaTuple_DsstDsPi0 = pi_ID_DsstDsPi0 + "&&" + Kmi_ID_DsstDsPi0 + "&&" + Kpl_ID_DsstDsPi0 + "&&" + gamma_ID_DsstDsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds1DsstPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_Ds1DsstPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433 && abs(pi_MC_GD_GD_MOTHER_ID) == 20433"
    Kmi_ID_Ds1DsstPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433 && abs(Kmi_MC_GD_GD_MOTHER_ID) == 20433"
    Kpl_ID_Ds1DsstPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433 && abs(Kpl_MC_GD_GD_MOTHER_ID) == 20433"
    gamma_ID_Ds1DsstPi0 = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_GD_MOTHER_ID) == 20433"

    TRUEID_DsGammaTuple_Ds1DsstPi0 = pi_ID_Ds1DsstPi0 + "&&" + Kmi_ID_Ds1DsstPi0 + "&&" + Kpl_ID_Ds1DsstPi0 + "&&" + gamma_ID_Ds1DsstPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    TRUEID_DsGammaTuple = {"Ds0DsPi0": TRUEID_DsGammaTuple_Ds0DsPi0, "DsstDsPi0": TRUEID_DsGammaTuple_DsstDsPi0, "Ds1DsstPi0": TRUEID_DsGammaTuple_Ds1DsstPi0,
                           "Ds1DsGamma": TRUEID_DsGammaTuple_Ds1DsGamma, "DsstDsGamma": TRUEID_DsGammaTuple_DsstDsGamma}
    #=============================================================================================================================================================================================================================
    
    # "D2KKPiPi0RTuple"
    #=============================================================================================================================================================================================================================

    # DsKKPiPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi0_ID_Ds = "abs(g1_TRUEID) == 22 && abs(g2_TRUEID) == 22 && abs(g1_MC_MOTHER_ID) == 111 && abs(g2_MC_MOTHER_ID) == 111 && abs(g1_MC_GD_MOTHER_ID) == 431 && abs(g2_MC_GD_MOTHER_ID) == 431"
    pi_ID_Ds  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431"
    Kmi_ID_Ds = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431"
    Kpl_ID_Ds = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431"

    TRUEID_D2KKPiPi0RTuple_DsKKPiPi0 = pi0_ID_Ds + "&&" + pi_ID_Ds + "&&" + Kmi_ID_Ds + "&&" + Kpl_ID_Ds
    #----------------------------------------------------------------------------------------

    TRUEID_D2KKPiPi0RTuple = {"Ds2KKPiPi0": TRUEID_D2KKPiPi0RTuple_DsKKPiPi0}  
    #=============================================================================================================================================================================================================================

    TRUEID={"DsPi02ggTuple": TRUEID_DsPi02ggTuple, "DsGammaTuple": TRUEID_DsGammaTuple, "D2KKPiPi0RTuple": TRUEID_D2KKPiPi0RTuple}

    return TRUEID


"""
Class Particle for particle information
-name: str object
-ID: int object
-cuadrip: vector.Momentum4D object
-PID: float object
"""
class Particle:
    
    """
    Atributtes:
    -name: name of the particle
    -ID: PDG ID of the pdg
    -P: cuadrimomentum of the particle
    -PID: Particle IDentification propertie
    """
    def __init__(self, name, ID, cuadrip, PID = None):
        self.name = name
        self.ID = ID
        self.P = cuadrip
        self.PID = PID

    """
    Methods
    """

    """
    String definition
    """
    def __str__(self):
        s = "{0} Particle: {1}".format(self.name, self.P)
        return s
    
    """
    Reproduction definition
    """
    def __repr__(self):
        return str(self)
    
    """
    Measured mass of the particle MM = sqrt(E ** 2 - P ** 2)
    """
    def MM(self):
        cuadrip = self.P
        return cuadrip.M
    
    """
    Momentum combination of two particles
    -part2: Particle object
    """
    def combine(self, part2):
        p1 = self.P
        p2 = part2.P

        ptot = p1 + p2
        return ptot
    
    """
    Compares the mass of two particles and returns the most massive
    -part1: Particle object
    -part2: Particle object
    """
    def more_massive(part1, part2):
        m1 = part1.MM()
        m2 = part2.MM()

        if abs(part1.ID) != abs(part2.ID):
            if m1 > m2:
                return part1
            else:
                return part2


"""
Class CompactEvent for storing event storing event information from nTuples
-runNumber: int object
-eventNumber: int object
-nPVs: int object
-nVeloTracks: int object
-Particles: dict object. Its columns are list of Particle class objects
"""
class CompactEvent: 
    
    """
    Atributtes
    -runNr: Number of the run of the event
    -evtNr: Number of the event
    -nPVs: Number of reconstructed primary verteces of the event
    -nVeloTracks: Number of tracks in the VErtex LOcator of the event
    -Particles: Interesting particles of each event
    """
    def __init__(self, runNumber, eventNumber, nPVs, nVeloTracks, Particles):
        self.runNr = runNumber
        self.evtNr = eventNumber
        self.nPVs = nPVs
        self.nVeloTracks = nVeloTracks
        self.particles = Particles

    """
    Methods
    """
    
    """
    String definition
    """
    def __str__(self):
        s = "Event {0} in run {1}".format(self.evtNr, self.runNr)
        s = s + '\n'
        s = s + 'nPVs = {0}'.format(self.nPVs)
        s = s + '\n'
        s = s + 'nVeloTracks = {0}'.format(self.nVeloTracks)
        s = s + '\n'
        s = s + 'Particles:'
        s = s + '\n'
        s = s + str(self.particles)
        return s
    
    """
    Reproduction definition
    """
    def __repr__(self):
        return str(self)

    """
    Selects which events are compatible to mix with
    -evt2: CompactEvent object
    """
    def accept_mixed_event(self, evt2):
        if (self.evtNr == evt2.evtNr) : return False

        if (self.runNr != evt2.runNr) : return False

        if abs(self.nVeloTracks-evt2.nVeloTracks) > 5 : return False

        return True

"""
Function that, from a given run, returns an event as CompactEvent class object
-df: pd.DataFrame object (data)
-runNr: int object. Number of the run
-evtNr: int object. Number of the event
-parts_names: iterable of string objects. The number of the particles
-parts_ID: iterable of int objects. The ID of the particles
"""

def CompactEvent_def(df, runNr, evtNr, parts_names = ["Ds", "piz"], parts_ID = [111, 431]):

    df_evt = df.query("eventNumber == {0}".format(evtNr))
    
    nVeloTracks = (pd.unique(df_evt["nVeloTracks"]))[0]
    nPVs = (pd.unique(df_evt["nPVs"]))[0]

    parts = { } 

    for k in range(len(parts_names)):
        df_iparts = df_evt[[parts_names[k]+"_PX", parts_names[k]+"_PY", parts_names[k]+"_PZ", parts_names[k]+"_PE"]]

        iparts_info = df_iparts.drop_duplicates().to_numpy()

        iparts_P = [vec.obj(px=ipart[0], py=ipart[1], pz=ipart[2], E=ipart[3]) for ipart in iparts_info]

        parts[parts_names[k]] = [Particle(parts_names[k], parts_ID[k], ipart_P) for ipart_P in iparts_P]

    evt = CompactEvent(runNr, evtNr, nPVs, nVeloTracks, parts)

    return evt

"""
Function that, from a given run, returns all the events as Compact event objects
-df: pd.DataFrame object (data)
-runNr: int object. Number of the run
-parts_names: iterable of string objects. The number of the particles
-parts_ID: iterable of int objects. The ID of the particles
"""
def events_in_1_run(df, runNr, parts_names = ["piz", "Ds"], parts_ID = [111, 431]):

    eventNumber = pd.unique((df.query("runNumber=={0}".format(runNr)))["eventNumber"])

    events_in_run = [CompactEvent_def(df, runNr, evtNr, parts_names, parts_ID) for evtNr in eventNumber]

    return events_in_run


"""
Event Mixing algorithm
-events_in_run: list object. The elements are CompactEvent class objects
-comb_cut: float object. Value of the mass combination cut
-parts_names: iterable of string objects. The particles that we want to combine
"""
def event_mixing(events_in_run, comb_cut, parts_names = ["Ds", "piz"]):
    
    if parts_names[0] == parts_names[1]:
        evt_mix_fst = set([(x, y) for x in events_in_run for y in events_in_run if CompactEvent.accept_mixed_event(x, y)])
        evt_mix = list(set((a, b) if a.evtNr <= b.evtNr else (b,a) for a,b in evt_mix_fst))

    else:
        evt_mix = [(x, y) for x in events_in_run for y in events_in_run if CompactEvent.accept_mixed_event(x, y)]

    evt_pairs = [ ]; mixed_masses = [ ]; part1_mass = [ ]; part2_mass = [ ]; mixed_masses_2 = [ ]

    for evt_pair in evt_mix:
        evt1, evt2 = evt_pair

        parts1_evt1 = (evt1.particles)[parts_names[0]]
        
        parts2_evt2 = (evt2.particles)[parts_names[1]]
     
        comb_MM = [(Particle.combine(p1, p2)).M for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]  

        comb_M = [(Particle.combine(p1, p2)).M-p1.MM()+135-p2.MM()+1969 for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]  

        parts1_M = [p1.MM() for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]

        parts2_M = [p2.MM() for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]

        evt_pair_name = [str(evt1.evtNr)+" U "+str(evt2.evtNr) for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]

        evt_pairs += evt_pair_name; mixed_masses += comb_MM; part1_mass += parts1_M; part2_mass += parts2_M; mixed_masses_2 += comb_M

    mixed_events = {"evtPair": evt_pairs,
                    parts_names[0] + parts_names[1] + "_MM": mixed_masses,
                    parts_names[0] + parts_names[1] + "_M": mixed_masses_2,
                    parts_names[0] + "_MM": part1_mass,
                    parts_names[1] + "_MM": part2_mass}

    return mixed_events
