"""
Module designed by Ivan Cambon Bouzas 
PhD student at Instituto Galego de Fisica de Altas Enerxias (IGFAE)
This module includes functions that are thought as shortcuts for ROOT coding in python
Any doubt or suggestion, check https://gitlab.cern.ch/jcambonb
"""

import ROOT
import numpy as np
import uncertainties as un
import pandas as pd 
import vector as vec
from particle import Particle
from hep_ml import reweight
from hep_ml.metrics_utils import ks_2samp_weighted
import matplotlib.pyplot as plt
import os

macro_path = "/home3/ivan.cambon/Cpp_Macros"
ROOT.gROOT.ProcessLine(f".L {macro_path}/HEP_Exp_Tools/Kinematics.C")

"""
Function for converting RDataFrame into pandas DataFrame
-tdf: RDataFrame object
-columns: iterable which elements correspond to branches of the RDataFrame. It is recommended to not use all the branches.
"""
def rdf_to_pdf(tdf, columns):
    np_tdf = tdf.AsNumpy(columns)
    pdf_tdf = pd.DataFrame(np_tdf)
    return pdf_tdf

"""
Function for converting pandas DataFrame into RDataFrame
-pdf: Pandas DataFrame object
"""
def pdf_to_rdf(pdf):
    columns = pdf.columns
    np_pdf = {key: pdf[key].values for key in columns}
    tdf = ROOT.RDF.FromNumpy(np_pdf)
    return tdf

"""
Function for, from an input ntuple, get an output ntuple with only the branches we want to keep
-input_file_path: str. Where the input file is located
-ttree_name: str. Name of the TTree inside the input file
-output_file_path: str. Where the output file will be located
-branches_to_keep: iterable whose elements are str. Names of the branches to keep
"""
def ntuple_branch_reductor(input_file_path, ttree_name, output_file_path, branches_to_keep):
    input_file = ROOT.TFile(input_file_path)
    if not input_file or input_file.IsZombie():
        print("Error: Could not open input file:", input_file_path)
        return

    input_tree = input_file.Get(ttree_name)
    if not input_tree:
        print("Error: Could not retrieve input TTree from file:", input_file_path)
        input_file.Close()
        return

    output_file = ROOT.TFile(output_file_path, "RECREATE")
    if not output_file or output_file.IsZombie():
        print("Error: Could not create output file:", output_file_path)
        input_file.Close()
        return

    input_tree.SetBranchStatus('*', 0)
    
    for branch_name in branches_to_keep:
        if not hasattr(input_tree, branch_name):
            continue
        input_tree.SetBranchStatus(branch_name, 1)

    output_tree = input_tree.CloneTree(-1)

    output_tree.SetDirectory(output_file)
    output_tree.Fill()

    output_file.Write()

    output_file.Close()
    input_file.Close()

"""
Function which creates a variable all nCandidate2, a new version of nCandidate once a selection is made in RDataFrames created with LHCb nTuples
-tdf: RDataFrame Object
-columns: List of str which corresponds to a little set of variables included in tdf. If columns is large, it will take a while to run
"""
def nCandidate2_def(tdf, columns):
    np_tdf = tdf.AsNumpy(columns)
    runs_and_evts = list(zip(np_tdf['runNumber'], np_tdf['eventNumber']))
        
    count_dict = {}
    for x in runs_and_evts:
        count_dict[x] = count_dict.get(x, 0) + 1

    cand2_array = np.array([])
    totc2_array = np.array([])
    
    for x in count_dict.keys():
        counts = count_dict[x]
        aux1 = np.arange(counts)
        aux2 = counts * np.ones(counts)
        
        cand2_array = np.append(cand2_array, aux1)
        totc2_array = np.append(totc2_array, aux2)

    np_tdf["nCandidate2"] = cand2_array.astype("int")
    np_tdf["totCandidates2"] = totc2_array.astype("int")
    
    tdf_new = ROOT.RDF.FromNumpy(np_tdf)

    return tdf_new

"""
Function which performs a Figure of Merits (FoM) for 1 variable in order to get its optimal cut through the signifiance (S/sqrt(S+B)) criteria
Returns plots of significance and purity (S/B)
-sig_rdf: RDataFrame object which corresponds to the signal dataset
-bkg_rdf: RDataFrame object which corresponds to the background dataset
-var: str which is the name of the variable that appears in the nTuples 
-nsig: float. signal yield without variable cut applied
-nbkg: float. backgroudn yield without variable cut applied
-var_threshold: float. The max value of the variable where the FoM is calculated
-ndots: int object. Sets the number of cuts to be tested
-cut_type: str object which sets the type of cut (greater or lower)
-xlabel: str object. It will be the xlabel of the FoM plots
-title: str object. Title for the plot
-label_plot: str object. Label for the legend
-fmt: str object. Matplotlib format for plot style
"""
def FoM1D_plot(sig_rdf, bkg_rdf, var, var_threshold, nsig, nbkg, 
               ndots=30, cut_type=">",
               xlabel="feature", title=None, label_plot=None,
               fmt="k."):
    
    var_min = min(sig_rdf.Min(var).GetValue(), bkg_rdf.Min(var).GetValue())
    var_max = max(sig_rdf.Max(var).GetValue(), bkg_rdf.Max(var).GetValue())

    var_vals = np.linspace(var_min, var_threshold, ndots)

    if cut_type == ">":
      var_cuts = [f"{var} > {var_vals[i]}" for i in range(len(var_vals))]

    if cut_type == "<":
      var_cuts = [f"{var} < {var_vals[i]}" for i in range(len(var_vals))]

    sig_raw = sig_rdf.Histo1D(("", "", 100, var_min, var_max), var)
    bkg_raw = bkg_rdf.Histo1D(("", "", 100, var_min, var_max), var)

    sig_rdfs_cut = [sig_rdf.Filter(var_cuts[i]).Histo1D(("", "", 100, var_min, var_max), var) for i in range(len(var_cuts))]
    bkg_rdfs_cut = [bkg_rdf.Filter(var_cuts[i]).Histo1D(("", "", 100, var_min, var_max), var) for i in range(len(var_cuts))]
    
    sig_prob = np.array([sig_rdf_cut.Integral() / sig_raw.Integral() for sig_rdf_cut in sig_rdfs_cut])
    bkg_prob = np.array([bkg_rdf_cut.Integral() / bkg_raw.Integral() for bkg_rdf_cut in bkg_rdfs_cut])

    S = nsig * sig_prob
    B = nbkg * bkg_prob
    Significance = S / np.sqrt(S+B)
    Purity = S / B

    i = np.argmax(Significance)
    f = np.argmax(Purity)

    print(f"{var} cut for maximum significance = {var_cuts[i]}")
    print(f"{var} cut for maximum purity= {var_cuts[f]}")

    plt.plot(var_vals, Significance, fmt, label=label_plot)
    plt.xlabel(xlabel)
    plt.ylabel(r'Figure of Merits $\frac{S}{\sqrt{S+B}}$')
    plt.title(title)
        
        
"""
Function that calculates the segmentation in intervals of a variable. It uses the cumulative distribution criteria
-df: Pandas DataFrame
-var: Python string. Name of the variable that we want to segmentate
-ninters: Python int. Number of intervals
-bins: Python int. Number of bins of the variable histogram
-weigths: Python string. Number of a variable that it will be used as weights for the histogram
-fmt: Python string. Format for matplotlib.pyplot.plot 
-xlabel: Python string. Name for matplotlib.pyplot.xlabel
-file_name: Python string. Name of the output graph
"""
def var_interval_inference(df, var, ninters, 
                           bins=100, weights=None, fmt="b-", xlabel="feature",
                           fig_name=None, file_name=None):
    if weights:
        var_hist, var_bins = np.histogram(df[var], bins=bins, density=True, weights=df[weights])
    else:
        var_hist, var_bins = np.histogram(df[var], bins=bins, density=True)

    var_cdf = np.cumsum(var_hist * np.diff(var_bins))
    
    cdf_equi_vals = np.linspace(0, var_cdf[-1], ninters + 1)  # Equally spaced CDF values
    interval_edges = [np.min(df[var])]
    
    for equi_val in cdf_equi_vals:
        if equi_val != 0:
            cdf_xy = zip(var_cdf, var_bins[:-1])
            var_inter_vals = [y for x,y in cdf_xy if x < equi_val]
            interval_edges.append(var_inter_vals[-1])

    interval_edges = np.array(interval_edges)
    plt.figure(1)
    plt.plot(var_bins[1:], var_cdf, fmt)
    plt.xlabel(xlabel, fontsize=14)
    plt.title('Cumulative Distribution Function')
    for edge, cap in zip(interval_edges, cdf_equi_vals):
        plt.axvline(x=edge, color='black', linestyle=':', linewidth=1)
        plt.axhline(y=cap, color='black', linestyle=':', linewidth=1)
        
    print("Interval Boundaries:")
    for i in range(len(interval_edges) - 1):
        print(f"Interval {i + 1}: [{interval_edges[i]:.2f}, {interval_edges[i + 1]:.2f}]")
        
    if fig_name:
        plt.savefig(fig_name, dpi=300, bbox_inches='tight')
    
    if file_name:
        np.savetxt(file_name, interval_edges)
    
    return interval_edges
        
"""
Function to reweight a given distribution throught the bin-based reweighting method
-tdf_original: RDataFrame object. The sample we want to reweight
-tdf_target: RDataFrame object. The sample we use as reference to compute the weights
-columns: list whose elements are str objects. Names of the feature we want to include
-var: str object. Name of the feature we want to reweight
-bins: int object. Number of bins used for computing the weights
-df_flab: bool. Allows you switch between pandas DataFrame output or RDataFrame output
"""       
def bin_based_reweight(tdf_original, tdf_target, columns, var, 
                       bins=100, df_flag=True):
    
    df_original = rdf_to_pdf(tdf_original, columns=columns)
    df_target = rdf_to_pdf(tdf_target, columns=columns)
        
    bins_reweighter = reweight.BinsReweighter(n_bins=bins, n_neighs=1.)
    
    if var in columns:
        df_original_var = df_original[var]
        df_target_var = df_target[var]
        
        bins_reweighter.fit(df_original_var, df_target_var)
        
        bins_weights = bins_reweighter.predict_weights(df_original_var)
        
        df_original["{0}_mult_bbw".format(var)] = bins_weights
        
        print("KS test over MC distributions")
        print("---------------------------------------------------------------------------")
        print("Before reweighting")
        print('KS over ', var, ' = ', 
        ks_2samp_weighted(df_original_var, df_target_var,
                          weights1=np.ones(len(df_original_var)),
                          weights2=np.ones(len(df_target_var)))
        )
        print("After reweighting")
        print('KS over ', var, ' = ', 
        ks_2samp_weighted(df_original_var, df_target_var,
                          weights1=bins_weights,
                          weights2=np.ones(len(df_target_var)))
        )
        print("---------------------------------------------------------------------------")
        
        if df_flag:
            return df_original
        else:
            
            tdf_original_new = pdf_to_rdf(df_original)
            
            return tdf_original_new
    
    else:
        print("Feature not included in columns list")
        
        
        
"""
Basic function for histogram TH1D plotting. Line or Marker options are included
For RooFit plotting check the RooPyFit module
-hist: TH1D object
-min_val: float object. Minimum y axis value
-max_val: float object. Maximum y axis value
-opt: str object. Option for TH1D plotting
-norm: bool. Draw normalized by Nentries
-density: bool. Draw normalized by Integral
-color: int or ROOT.TColor. Color for the histogram plot
-err_color: int or ROOT.TColor. Color for the errors in E style TH1D plot
-linestyle: int or ROOT.TAttLine. Style for line histogram plot
-markerstyle: int or ROOT.TAttMarker. Style for marker histogram plot
-fillstyle: int or ROOT.TAttFill. Style for bar histogram plot
-xlabel: str for xlabel
-ylabel: str for ylabel
-title: str for title
"""
def TH1D_plot(hist, 
              min_val=0., max_val=None,
              opt="HIST same", norm=False, density=False,
              color=1, err_color=None, linestyle=20, markerstyle=20, fillstyle=1001,
              xlabel="feature", ylabel="A.U", title="title"):
    
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)
    hist.SetTitle(title)
    
    hist.SetMinimum(min_val)
    if max_val:
        hist.SetMaximum(max_val)

    if "E" in opt:
        hist.SetMarkerStyle(markerstyle)
        hist.SetMarkerColor(color)
        if err_color:
            hist.SetLineColor(err_color)
        else:
            hist.SetLineColor(color)
    
    if opt == "same" or "HIST" in opt:
        hist.SetLineStyle(linestyle)
        hist.SetLineColor(color)
        
    if "BAR" in opt:
        hist.SetFillColor(color)
        hist.SetFillStyle(fillstyle)
        
    if density:
        I = hist.Integral()
        hist_copy = hist.Copy()
        hist_copy.Scale(1./I)
        hist_copy.Draw()
    
    if norm:
        hist.DrawNormalized(opt)
        
    else:
        hist.Draw(opt)


"""
Basic function for histogram TH2D plotting. SCAT or BOX options are included
For RooFit plotting check the RooPyFit module
-hist: TH2D object
-opt: str object. Option for TH2D plotting
-norm: bool. Draw normalized by Nentries
-density: bool. Draw normalized by Integral
-color: int or ROOT.TColor. Color for the scatter plot
-markerstyle: int or ROOT.TAttMarker. Style for scatter plot
-xlabel: str for xlabel
-ylabel: str for ylabel
-title: str for title
"""
def TH2D_plot(hist, 
              opt="COLZ same", norm=False, density=False,
              color=1, markerstyle=20, 
              xlabel="feature1", ylabel="feature2", title="title"):
    
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)
    hist.SetTitle(title)

    if 'SCAT' in opt:
        hist.SetMarkerStyle(markerstyle)
        hist.SetMarkerColor(color)  

    if 'BOX' in opt:
        hist.SetFillColor(color)

    if density:
        I = hist.Integral()
        hist_copy = hist.Copy()
        hist_copy.Scale(1./I)
        hist_copy.Draw()
    
    if norm:
        hist.DrawNormalized(opt)
        
    else:
        hist.Draw(opt)
   
"""
Basic function for histogram TProfile plotting. Line or Marker options are included
For RooFit plotting check the RooPyFit module
-prof: TProfile object
-opt: str object. Option for TProfile object
-color: int or ROOT.TColor. Color for the markers
-ecolor: int or ROOT.TColor. Color for the erros
-markerstyle: int or ROOT.TAttMarker. Style for markers
-xlabel: str for xlabel
-ylabel: str for ylabel
-title: str for title
"""
def TProfile_plot(prof, 
                  opt="E1 same", color=1, ecolor=1, markerstyle=20,
                  xlabel="feature1", ylabel="feature2", title="title"):
    
    prof.GetXaxis().SetTitle(xlabel)
    prof.GetYaxis().SetTitle(ylabel)
    prof.SetTitle(title)

    prof.SetLineColor(ecolor)
    
    prof.SetMarkerStyle(markerstyle)
    prof.SetMarkerColor(color)

    prof.Draw(opt)
    
"""
Basic function for TF1 fitting and plotting. Line or Marker options are included
For RooFit plotting check the RooPyFit module
-fun: TF1 object
-data: ROOT object with Fit attribute: TGraph, TH1D, TProfile, etc.
-color: ROOT line color
-linestyle: ROOT line style
-linewidth: ROOT line width
"""
def curve_fit(fun, data,
              color=1, linestyle=1, linewidth=1):
    
    data.Fit(fun)
    
    f = data.GetFunction(fun.GetName())
    f.SetLineColor(color)
    f.SetLineStyle(linestyle)
    f.SetLineWidth(linewidth)
    
"""
Function for legend plotting in usual TCanvas. 
For RooFit plotting check the RooPyFit module
-lgd: TLegend object
-hlist: iterable object whose elements are Plotable objects. For TH1D comming for RDataFrame, you must use h.GetPtr()
-lgd_names: iterable object whose elements are the names that we want to put in the legend
-styles: iterable object whose elements are the styles of the legend
"""
def legend_plot(lgd, hlist, lgd_names, styles):
    for i in range(len(hlist)):
        lgd.AddEntry(hlist[i], lgd_names[i], styles[i])

    lgd.SetBorderSize(0)
    lgd.SetFillStyle(0)
    lgd.Draw()

"""
Function to save a ROOT histogram inside a root file. We use the name of the file without .root as the name of the histogram saved inside the file
-file_name: str object. Name of the file without .root
-hist: TH1 object
"""
def save_hist_to_root(hist,
                      file_name="MyHist.root"):
    t = ROOT.TFile.Open(file_name, "RECREATE")
    hist.Write("MyHist")
    t.Close()

"""
Function to read a ROOT histogram inside a root file. The name of the histogram must be the same as the file name without .root
-file_name: str object. Name of the root file
"""
def read_hist_to_root(file_path):
    file = ROOT.TFile(file_path, "READ")
    hist = file.Get("MyHist")
    
    if hist:
       return hist
    else:
        print(f"Histogram not found in '{file_path}'.")
        return None
    
"""
Function that has MCmatching for DsJ states vs multiplicity analysis
"""
def TRUEIDs():

    # DsPi02ggTuple and DsstGammaTuple
    #=============================================================================================================================================================================================================================
    # Ds0DsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DspizL_Ds0DsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 10431"
    Kmi_ID_DspizL_Ds0DsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 10431"
    Kpl_ID_DspizL_Ds0DsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 10431"
    piz_ID_DspizL_Ds0DsPi0 = "abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22 && abs(gamma_1_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_GD_MOTHER_ID) == 10431 && abs(gamma_1_MC_GD_MOTHER_ID) == 10431"
  
    TRUEID_DsPi02ggTuple_Ds0DsPi0 = "{0} && {1} && {2} && {3}".format(pi_ID_DspizL_Ds0DsPi0, 
                                                                      Kmi_ID_DspizL_Ds0DsPi0, 
                                                                      Kpl_ID_DspizL_Ds0DsPi0,
                                                                      piz_ID_DspizL_Ds0DsPi0)
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # DsstDsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DspizL_DsstDsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DspizL_DsstDsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DspizL_DsstDsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    piz_ID_DspizL_DsstDsPi0 = "abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22 && abs(gamma_1_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_MOTHER_ID) == 111 && abs(gamma_2_MC_GD_MOTHER_ID) == 433 && abs(gamma_1_MC_GD_MOTHER_ID) == 433"

    TRUEID_DsPi02ggTuple_DsstDsPi0 = "{0} && {1} && {2} && {3}".format(pi_ID_DspizL_DsstDsPi0, 
                                                                       Kmi_ID_DspizL_DsstDsPi0, 
                                                                       Kpl_ID_DspizL_DsstDsPi0, 
                                                                       piz_ID_DspizL_DsstDsPi0)  
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
    # Ds1DsstPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DspizL_Ds1DsstPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433 && abs(pi_MC_GD_GD_MOTHER_ID) == 20433"
    Kmi_ID_DspizL_Ds1DsstPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433 && abs(Kmi_MC_GD_GD_MOTHER_ID) == 20433"
    Kpl_ID_DspizL_Ds1DsstPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433 && abs(Kpl_MC_GD_GD_MOTHER_ID) == 20433"
    gg_ID_DspizL_Ds1DsstPi0 = "abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22 && abs(gamma_2_MC_GD_MOTHER_ID) == 20433 && abs(gamma_1_MC_GD_MOTHER_ID) == 20433"


    TRUEID_DsPi02ggTuple_Ds1DsstPi0 = "{0} && {1} && {2} && {3}".format(pi_ID_DspizL_Ds1DsstPi0,
                                                                        Kmi_ID_DspizL_Ds1DsstPi0,
                                                                        Kpl_ID_DspizL_Ds1DsstPi0, 
                                                                        gg_ID_DspizL_Ds1DsstPi0)  
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # DsstDsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DspizL_DsstDsGamma  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DspizL_DsstDsGamma = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DspizL_DsstDsGamma = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    gg_ID_DspizL_DsstDsGamma = "(abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22) && (abs(gamma_1_MC_MOTHER_ID) == 433 | abs(gamma_2_MC_MOTHER_ID) == 433)"


    TRUEID_DsPi02ggTuple_DsstDsGamma = "{0} && {1} && {2} && {3}".format(pi_ID_DspizL_DsstDsGamma, 
                                                                         Kmi_ID_DspizL_DsstDsGamma, 
                                                                         Kpl_ID_DspizL_DsstDsGamma, 
                                                                         gg_ID_DspizL_DsstDsGamma)  
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds1DsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DspizL_Ds1DsGamma  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 20433"
    Kmi_ID_DspizL_Ds1DsGamma = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 20433"
    Kpl_ID_DspizL_Ds1DsGamma = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 20433"
    gg_ID_DspizL_Ds1DsGamma = "(abs(gamma_1_TRUEID) == 22 && abs(gamma_2_TRUEID) == 22) && (abs(gamma_1_MC_MOTHER_ID) == 20433 | abs(gamma_2_MC_MOTHER_ID) == 20433)"


    TRUEID_DsPi02ggTuple_Ds1DsGamma =  "{0} && {1} && {2} && {3}".format(pi_ID_DspizL_Ds1DsGamma, 
                                                                         Kmi_ID_DspizL_Ds1DsGamma, 
                                                                         Kpl_ID_DspizL_Ds1DsGamma, 
                                                                         gg_ID_DspizL_Ds1DsGamma)  
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    TRUEID_DsPi02ggTuple = {"Ds0DsPi0": TRUEID_DsPi02ggTuple_Ds0DsPi0, 
                            "DsstDsPi0": TRUEID_DsPi02ggTuple_DsstDsPi0, 
                            "Ds1DsstPi0": TRUEID_DsPi02ggTuple_Ds1DsstPi0, 
                            "Ds1DsGamma": TRUEID_DsPi02ggTuple_Ds1DsGamma, 
                            "DsstDsGamma": TRUEID_DsPi02ggTuple_DsstDsGamma}
    #=============================================================================================================================================================================================================================

    # DsGammaTuple
    #=============================================================================================================================================================================================================================
    # DsstDsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Ds_ID_DsgL_DsstDsGamma = "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 433"
    gamma_ID_DsgL_DsstDsGamma = "abs(gamma_TRUEID) == 22 && (abs(gamma_MC_MOTHER_ID) == 433 | abs(gamma_MC_GD_MOTHER_ID) == 433 | abs(gamma_MC_GD_GD_MOTHER_ID) == 433)"
    

    TRUEID_DsGammaTuple_DsstDsGamma = f"{Ds_ID_DsgL_DsstDsGamma} && {gamma_ID_DsgL_DsstDsGamma}"
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds1DsGamma
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Ds_ID_DsgL_Ds1DsGamma = "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 20433"
    gamma_ID_DsgL_Ds1DsGamma = "abs(gamma_TRUEID) == 22 && (abs(gamma_MC_MOTHER_ID) == 20433 | abs(gamma_MC_GD_MOTHER_ID) == 20433 | abs(gamma_MC_GD_GD_MOTHER_ID) == 20433)"
    

    TRUEID_DsGammaTuple_Ds1DsGamma = f"{Ds_ID_DsgL_Ds1DsGamma} && {gamma_ID_DsgL_Ds1DsGamma}"
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds0DsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsgL_Ds0DsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 10431"
    Kmi_ID_DsgL_Ds0DsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 10431"
    Kpl_ID_DsgL_Ds0DsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 10431"
    gamma_ID_DsgL_Ds0DsPi0 = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_GD_MOTHER_ID) == 10431"

    TRUEID_DsGammaTuple_Ds0DsPi0 = "{0} && {1} && {2} && {3}".format(pi_ID_DsgL_Ds0DsPi0, 
                                                                     Kmi_ID_DsgL_Ds0DsPi0, 
                                                                     Kpl_ID_DsgL_Ds0DsPi0, 
                                                                     gamma_ID_DsgL_Ds0DsPi0)
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # DsstDsPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsgL_DsstDsPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433"
    Kmi_ID_DsgL_DsstDsPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433"
    Kpl_ID_DsgL_DsstDsPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433"
    gamma_ID_DsgL_DsstDsPi0 = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_MOTHER_ID) == 111 && abs(gamma_MC_GD_MOTHER_ID) == 433"

    TRUEID_DsGammaTuple_DsstDsPi0 = "{0} && {1} && {2} && {3}".format(pi_ID_DsgL_DsstDsPi0, 
                                                                      Kmi_ID_DsgL_DsstDsPi0, 
                                                                      Kpl_ID_DsgL_DsstDsPi0, 
                                                                      gamma_ID_DsgL_DsstDsPi0)
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Ds1DsstPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi_ID_DsgL_Ds1DsstPi0  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431 && abs(pi_MC_GD_MOTHER_ID) == 433 && abs(pi_MC_GD_GD_MOTHER_ID) == 20433"
    Kmi_ID_DsgL_Ds1DsstPi0 = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431 && abs(Kmi_MC_GD_MOTHER_ID) == 433 && abs(Kmi_MC_GD_GD_MOTHER_ID) == 20433"
    Kpl_ID_DsgL_Ds1DsstPi0 = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431 && abs(Kpl_MC_GD_MOTHER_ID) == 433 && abs(Kpl_MC_GD_GD_MOTHER_ID) == 20433"
    gamma_ID_DsgL_Ds1DsstPi0 = "abs(gamma_TRUEID) == 22 && abs(gamma_MC_GD_MOTHER_ID) == 20433"

    TRUEID_DsGammaTuple_Ds1DsstPi0 = "{0} && {1} && {2} && {3}".format(pi_ID_DsgL_Ds1DsstPi0, 
                                                                       Kmi_ID_DsgL_Ds1DsstPi0, 
                                                                       Kpl_ID_DsgL_Ds1DsstPi0, 
                                                                       gamma_ID_DsgL_Ds1DsstPi0)
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    TRUEID_DsGammaTuple = {"Ds0DsPi0": TRUEID_DsGammaTuple_Ds0DsPi0, 
                           "DsstDsPi0": TRUEID_DsGammaTuple_DsstDsPi0, 
                           "Ds1DsstPi0": TRUEID_DsGammaTuple_Ds1DsstPi0,
                           "Ds1DsGamma": TRUEID_DsGammaTuple_Ds1DsGamma, 
                           "DsstDsGamma": TRUEID_DsGammaTuple_DsstDsGamma}
    #=============================================================================================================================================================================================================================
    
    # "D2KKPiPi0RTuple"
    #=============================================================================================================================================================================================================================

    # DsKKPiPi0
    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pi0_ID_DKKPiPi0_Ds = "abs(g1_TRUEID) == 22 && abs(g2_TRUEID) == 22 && abs(g1_MC_MOTHER_ID) == 111 && abs(g2_MC_MOTHER_ID) == 111 && abs(g1_MC_GD_MOTHER_ID) == 431 && abs(g2_MC_GD_MOTHER_ID) == 431"
    pi_ID_DKKPiPi0_Ds  = "abs(pi_TRUEID) == 211 && abs(pi_MC_MOTHER_ID) == 431"
    Kmi_ID_DKKPiPi0_Ds = "abs(Kmi_TRUEID) == 321 && abs(Kmi_MC_MOTHER_ID) == 431"
    Kpl_ID__DKKPiPi0_Ds = "abs(Kpl_TRUEID) == 321 && abs(Kpl_MC_MOTHER_ID) == 431"

    TRUEID_D2KKPiPi0RTuple_DsKKPiPi0 = "{0} && {1} && {2} && {3}".format(pi0_ID_DKKPiPi0_Ds, 
                                                                         pi_ID_DKKPiPi0_Ds, 
                                                                         Kmi_ID_DKKPiPi0_Ds, 
                                                                         Kpl_ID__DKKPiPi0_Ds)
    #----------------------------------------------------------------------------------------

    TRUEID_D2KKPiPi0RTuple = {"Ds2KKPiPi0": TRUEID_D2KKPiPi0RTuple_DsKKPiPi0}  
    #=============================================================================================================================================================================================================================

    TRUEID = {"DsPi02ggTuple": TRUEID_DsPi02ggTuple, 
              "DsGammaTuple": TRUEID_DsGammaTuple, 
              "D2KKPiPi0RTuple": TRUEID_D2KKPiPi0RTuple}

    return TRUEID


"""
Class Particle for particle information
-name: str object
-ID: int object
-cuadrip: vector.Momentum4D object
-PID: float object
"""
class Particle:
    
    """
    Atributtes:
    -name: name of the particle
    -ID: PDG ID of the pdg
    -P: cuadrimomentum of the particle
    -PID: Particle IDentification propertie
    """
    def __init__(self, name, ID, cuadrip, PID = None):
        self.name = name
        self.ID = ID
        self.P = cuadrip
        self.PID = PID

    """
    Methods
    """

    """
    String definition
    """
    def __str__(self):
        s = "{0} Particle: {1}".format(self.name, self.P)
        return s
    
    """
    Reproduction definition
    """
    def __repr__(self):
        return str(self)
    
    """
    Measured mass of the particle MM = sqrt(E ** 2 - P ** 2)
    """
    def MM(self):
        cuadrip = self.P
        return cuadrip.M
    
    """
    Momentum combination of two particles
    -part2: Particle object
    """
    def combine(self, part2):
        p1 = self.P
        p2 = part2.P

        ptot = p1 + p2
        return ptot
    
    """
    Compares the mass of two particles and returns the most massive
    -part1: Particle object
    -part2: Particle object
    """
    def more_massive(part1, part2):
        m1 = part1.MM()
        m2 = part2.MM()

        if abs(part1.ID) != abs(part2.ID):
            if m1 > m2:
                return part1
            else:
                return part2

"""
Class CompactEvent for storing event storing event information from nTuples
-runNumber: int object
-eventNumber: int object
-nPVs: int object
-nVeloTracks: int object
-Particles: dict object. Its columns are list of Particle class objects
"""
class CompactEvent: 
    
    """
    Atributtes
    -runNr: Number of the run of the event
    -evtNr: Number of the event
    -nPVs: Number of reconstructed primary verteces of the event
    -nVeloTracks: Number of tracks in the VErtex LOcator of the event
    -Particles: Interesting particles of each event
    """
    def __init__(self, runNumber, eventNumber, nPVs, nVeloTracks, Particles):
        self.runNr = runNumber
        self.evtNr = eventNumber
        self.nPVs = nPVs
        self.nVeloTracks = nVeloTracks
        self.particles = Particles

    """
    Methods
    """
    
    """
    String definition
    """
    def __str__(self):
        s = "Event {0} in run {1}".format(self.evtNr, self.runNr)
        s = s + '\n'
        s = s + 'nPVs = {0}'.format(self.nPVs)
        s = s + '\n'
        s = s + 'nVeloTracks = {0}'.format(self.nVeloTracks)
        s = s + '\n'
        s = s + 'Particles:'
        s = s + '\n'
        s = s + str(self.particles)
        return s
    
    """
    Reproduction definition
    """
    def __repr__(self):
        return str(self)

    """
    Selects which events are compatible to mix with
    -evt2: CompactEvent object
    """
    def accept_mixed_event(self, evt2):
        if (self.evtNr == evt2.evtNr) : return False

        if (self.runNr != evt2.runNr) : return False

        if abs(self.nVeloTracks-evt2.nVeloTracks) > 5 : return False

        return True

"""
Function that, from a given run, returns an event as CompactEvent class object
-df: pd.DataFrame object (data)
-runNr: int object. Number of the run
-evtNr: int object. Number of the event
-parts_names: iterable of string objects. The number of the particles
-parts_ID: iterable of int objects. The ID of the particles
"""

def CompactEvent_def(df, runNr, evtNr, 
                     parts_names = ["Ds", "piz"], parts_ID = [111, 431]):

    df_evt = df.query("eventNumber == {0}".format(evtNr))
    
    nVeloTracks = (pd.unique(df_evt["nVeloTracks"]))[0]
    nPVs = (pd.unique(df_evt["nPVs"]))[0]

    parts = { } 

    for k in range(len(parts_names)):
        df_iparts = df_evt[[parts_names[k]+"_PX", parts_names[k]+"_PY", parts_names[k]+"_PZ", parts_names[k]+"_PE"]]

        iparts_info = df_iparts.drop_duplicates().to_numpy()

        iparts_P = [vec.obj(px=ipart[0], py=ipart[1], pz=ipart[2], E=ipart[3]) for ipart in iparts_info]

        parts[parts_names[k]] = [Particle(parts_names[k], parts_ID[k], ipart_P) for ipart_P in iparts_P]

    evt = CompactEvent(runNr, evtNr, nPVs, nVeloTracks, parts)

    return evt

"""
Function that, from a given run, returns all the events as Compact event objects
-df: pd.DataFrame object (data)
-runNr: int object. Number of the run
-parts_names: iterable of string objects. The number of the particles
-parts_ID: iterable of int objects. The ID of the particles
"""
def events_in_1_run(df, runNr, 
                    parts_names = ["piz", "Ds"], parts_ID = [111, 431]):

    eventNumber = pd.unique((df.query("runNumber=={0}".format(runNr)))["eventNumber"])

    events_in_run = [CompactEvent_def(df, runNr, evtNr, parts_names, parts_ID) for evtNr in eventNumber]

    return events_in_run


"""
Event Mixing algorithm
-events_in_run: list object. The elements are CompactEvent class objects
-comb_cut: float object. Value of the mass combination cut
-parts_names: iterable of string objects. The particles that we want to combine
"""
def event_mixing(events_in_run, comb_cut, 
                 parts_names = ["Ds", "piz"]):
    
    if parts_names[0] == parts_names[1]:
        evt_mix_fst = set([(x, y) for x in events_in_run for y in events_in_run if CompactEvent.accept_mixed_event(x, y)])
        evt_mix = list(set((a, b) if a.evtNr <= b.evtNr else (b,a) for a,b in evt_mix_fst))

    else:
        evt_mix = [(x, y) for x in events_in_run for y in events_in_run if CompactEvent.accept_mixed_event(x, y)]

    evt_pairs = [ ]; mixed_masses = [ ]; part1_mass = [ ]; part2_mass = [ ]; mixed_masses_2 = [ ]

    for evt_pair in evt_mix:
        evt1, evt2 = evt_pair

        parts1_evt1 = (evt1.particles)[parts_names[0]]
        
        parts2_evt2 = (evt2.particles)[parts_names[1]]
     
        comb_MM = [(Particle.combine(p1, p2)).M for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]  

        comb_M = [(Particle.combine(p1, p2)).M-p1.MM()+135-p2.MM()+1969 for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]  

        parts1_M = [p1.MM() for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]

        parts2_M = [p2.MM() for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]

        evt_pair_name = [str(evt1.evtNr)+" U "+str(evt2.evtNr) for p1 in parts1_evt1 for p2 in parts2_evt2 if (Particle.combine(p1, p2)).M-Particle.more_massive(p1, p2).MM() < comb_cut]

        evt_pairs += evt_pair_name; mixed_masses += comb_MM; part1_mass += parts1_M; part2_mass += parts2_M; mixed_masses_2 += comb_M

    mixed_events = {"evtPair": evt_pairs,
                    parts_names[0] + parts_names[1] + "_MM": mixed_masses,
                    parts_names[0] + parts_names[1] + "_M": mixed_masses_2,
                    parts_names[0] + "_MM": part1_mass,
                    parts_names[1] + "_MM": part2_mass}

    return mixed_events
